import { HospitalParams, HospitalTypes, HospitalActions } from '@interface'

export const requestPaymentMethod = ({
  partnerId
}: HospitalParams.PaymentMethod.Request): HospitalActions => {
  return {
    type: HospitalTypes.PaymentMethod.Request,
    payload: { partnerId }
  }
}

export const responsePaymentMethod = ({
  data
}: HospitalParams.PaymentMethod.Response): HospitalActions => {
  return {
    type: HospitalTypes.PaymentMethod.Response,
    payload: { data }
  }
}

export const resetPaymentMethod = () => {
  return {
    type: HospitalTypes.PaymentMethod.Reset
  }
}
export const selectPaymentFee = ({
  selectPaymentFee
}: HospitalParams.PaymentMethod.SelectPaymentFee): HospitalActions => {
  return {
    type: HospitalTypes.PaymentMethod.Select,
    payload: { selectPaymentFee }
  }
}
