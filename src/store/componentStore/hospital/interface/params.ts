export namespace HospitalParams {
  export namespace PaymentMethod {
    export interface Request {
      partnerId?: string
    }
    export interface Response {
      data: any
    }
    export interface SelectPaymentFee {
      selectPaymentFee: any
    }
  }
}
