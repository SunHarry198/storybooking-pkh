import { HospitalTypes, HospitalParams } from '@interface'
export type HospitalActions = PaymentMethodAction

export type PaymentMethodAction =
  | RequsetPaymentMethod
  | ResponsePaymentMethod
  | ResetPaymentMethod
  | SelectPaymentFee
export interface RequsetPaymentMethod {
  type: HospitalTypes.PaymentMethod.Request
  payload: HospitalParams.PaymentMethod.Request
}
export interface ResponsePaymentMethod {
  type: HospitalTypes.PaymentMethod.Response
  payload: HospitalParams.PaymentMethod.Response
}
export interface ResetPaymentMethod {
  type: HospitalTypes.PaymentMethod.Reset
}

export interface SelectPaymentFee {
  type: HospitalTypes.PaymentMethod.Select
  payload: HospitalParams.PaymentMethod.SelectPaymentFee
}
