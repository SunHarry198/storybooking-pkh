import { TotalDataParams, TotalDataTypes, TotalDataActions } from '@interface'
export const responseFilterCheckData = ({
  data
}: TotalDataParams.FilterCheckData.Response): TotalDataActions => {
  return {
    type: TotalDataTypes.FilterCheckData.Response,
    payload: { data }
  }
}

export const requsetUploadImage = ({
  data
}: TotalDataParams.UploadImage.Request): TotalDataActions => {
  return {
    type: TotalDataTypes.UploadImage.request,
    payload: { data }
  }
}

export const responseUploadImage = ({
  url
}: TotalDataParams.UploadImage.response): TotalDataActions => {
  return {
    type: TotalDataTypes.UploadImage.response,
    payload: { url }
  }
}

// GET BOOKING INFO
export const requestBookingInfo = ({
  idBooking,
  transactionId,
  smsCode
}: TotalDataParams.BookingInfo.Request): TotalDataActions => {
  return {
    type: TotalDataTypes.BookingInfo.Request,
    payload: { idBooking, transactionId, smsCode }
  }
}

export const responseBookingInfo = ({
  data
}: TotalDataParams.BookingInfo.Response): TotalDataActions => {
  return {
    type: TotalDataTypes.BookingInfo.Response,
    payload: { data }
  }
}
