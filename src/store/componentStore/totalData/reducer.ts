import { TotalDataActions, TotalDataState, TotalDataTypes } from '@interface'
import { HYDRATE } from 'next-redux-wrapper'

const init: TotalDataState = {
  filterCheckData: [],
  url: '',
  bookingInfo: undefined
}

export default function TotalData(
  state = init,
  action:
    | TotalDataActions
    | {
        type: typeof HYDRATE
        payload?: TotalDataState
      }
): TotalDataState {
  switch (action.type) {
    case TotalDataTypes.FilterCheckData.Response:
      return {
        ...state,
        filterCheckData: action.payload.data
      }
    case TotalDataTypes.UploadImage.response:
      return {
        ...state,
        url: action.payload.url
      }
    case TotalDataTypes.BookingInfo.Response:
      return {
        ...state,
        bookingInfo: action.payload.data
      }
    default:
      return state
  }
}
