import actionStore from '@actionStore'
import { TotalDataTypes } from '@interface'
import { client } from '@config/medproSDK'
import { all, fork, put, takeLatest } from 'redux-saga/effects'
import * as DTO from './interface/action'
import { AxiosResponse } from 'axios'

function* uploadImage({ payload }: DTO.RequsetUploadImage) {
  try {
    const response: AxiosResponse = yield client.uploadFile(
      { file: payload.data },
      {
        partnerid: 'dalieuhcm',
        appid: 'dalieuhcm',
        token:
          'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6IiIsInN1YiI6MCwidXNlck1vbmdvSWQiOiI2Mjk4NjEzYTdkZjUwYTAwMTkzMWNhNWYiLCJpYXQiOjE2NTg0ODQ1MjYsImV4cCI6NDgxNDI0NDUyNn0.JgbWfdT5GW33ta97EIJ_OiL1KQ30xyVqSeSUVaXXMYw'
      }
    )
    yield put(actionStore.responseUploadImage({ url: response }))
  } catch (error) {
    console.info(error)
  }
}
function* watcher_uploadImage() {
  yield takeLatest(TotalDataTypes.UploadImage.request, uploadImage)
}

// GET BOOKING INFO (SELECT BILL)

function* getBookingInfo({ payload }: DTO.RequestBookingInfo) {
  try {
    const response: AxiosResponse = yield client.getBookingInfoCSKH(
      {
        id: payload.idBooking,
        transactionId: payload.transactionId,
        smsCode: payload.smsCode
      },
      {
        token:
          'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6IiIsInN1YiI6MCwidXNlck1vbmdvSWQiOiI2MDllMmRhMmFjNjQxMTAwMTk2YWJiYWEiLCJpYXQiOjE2NjAyMDI1ODEsImV4cCI6NDgxNTk2MjU4MX0.Dwwjax2BVjHNKiKL8EqWt8QlxU-gYEHD7JcJH9Qh8N8',
        cskhtoken:
          'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWRQYXRpZW50IjoiNjA5ZTJkYTJhYzY0MTEwMDE5NmFiYmFhIiwicGF0aWVudElkIjoiOGQ1ZGUxNmRkZGZkNGQxNmE2NGQ3MzIyNTUzYjQyZDciLCJjc2toVXNlcklkIjoiNjA5ZTJkYTJhYzY0MTEwMDE5NmFiYmFhIiwiaWF0IjoxNjYwMjAyNTY5LCJleHAiOjE2NjAzNzUzNjl9.TGVJduHufeC32Lt03kb6LJNnxswht5kncTrD5rvplgY',
        partnerid: 'umcmono'
      }
    )
    const { data } = response

    // clog({ name: 'data', child: data, type: 'info' })
    if (data) {
      yield put(actionStore.responseBookingInfo({ data: data }))
    } else {
      yield put(actionStore.responseBookingInfo({ data: undefined }))
    }
  } catch (error) {
    // clog({ name: 'getBookingInfo', child: error, type: 'error' })
    yield put(actionStore.responseBookingInfo({ data: undefined }))
  }
}

function* watcher_requestBookingInfo() {
  yield takeLatest(TotalDataTypes.BookingInfo.Request, getBookingInfo)
}

const totalData = function* root() {
  yield all([fork(watcher_uploadImage)])
  yield all([fork(watcher_requestBookingInfo)])
}
export default totalData
