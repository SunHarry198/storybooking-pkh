export namespace TotalDataTypes {
  export enum FilterCheckData {
    Response = 'ResponseFilterCheck -> Response'
  }
  export enum UploadImage {
    request = 'UploadImage -> request',
    response = 'UploadImage -> response'
  }
  export enum BookingInfo {
    Request = 'Bookinginfo -> Request',
    Response = 'Bookinginfo -> Response',
    Reset = 'Bookinginfo -> Reset'
  }
}
