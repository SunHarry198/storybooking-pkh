export namespace FilterTypes {
  export enum Filter {
    Check = 'Filter -> Check',
    GetNextStep = 'Filter -> GetNextStep'
  }
}
