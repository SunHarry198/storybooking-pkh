export namespace FilterParams {
  export namespace Filter {
    export interface Check {
      isValid: string
    }
    export interface GetNextStep {
      id?: string
      insurance?: string
      plattform?: string
      typeNext?: string
      urlImg?: string
      ngayHenTaiKham?: string
      thoiDiemBHYT?: string
      partnerid: string
    }
  }
}
