import filterGuide from '@componentStore/filter/saga'
import hospitalSaga from '@componentStore/hospital/saga'
import totalData from '@componentStore/totalData/saga'
import { all, fork } from 'redux-saga/effects'

export default function* rootSaga(): Generator {
  yield all([fork(filterGuide)])
  yield all([fork(totalData)])
  yield all([fork(hospitalSaga)])
}
