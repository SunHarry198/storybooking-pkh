import NodeRSA from 'node-rsa'
const key = new NodeRSA(['huyi', ['pkcs8']])
export const encode = (text: any) => {
  const encrypted = key.encrypt(text, 'base64')
  return encrypted
}
export const decode = (text: any) => {
  try {
    const decrypted = key.decrypt(text, 'utf8')
    return decrypted
  } catch (error) {
    return null
  }
}
