import { clog } from '@lib/support/clog'
import { currentEnv } from '@config/envs'
import { VERSION } from '@config/version'

export const getPlatform = () => {
  if (window.innerWidth < 768.98) {
    return 'web'
  } else {
    return 'pc'
  }
}

export const getTypeOS = (userAgent: any) => {
  if (/windows phone/i.test(userAgent)) {
    return 'Windows Phone'
  }

  if (/android/i.test(userAgent)) {
    return 'android'
  }

  if (/iPad|iPhone|iPod/.test(userAgent)) {
    return 'ios'
  }
}

export const serviceWorker = () => {
  if ('serviceWorker' in navigator) {
    window.addEventListener('load', () => {
      navigator.serviceWorker.register('/sw.js').then(
        (registration) => {
          clog({
            name: 'Service Worker registration successful with scope',
            child: registration.scope,
            type: 'info'
          })

          const child = { version: VERSION, env: currentEnv }

          clog({
            name: 'Info System App',
            child: child,
            type: 'info'
          })

          window.localStorage.setItem('info ', JSON.stringify(child))
        },
        (err) => {
          console.info('Service Worker registration failed: ', err)
        }
      )
    })
  }
}
