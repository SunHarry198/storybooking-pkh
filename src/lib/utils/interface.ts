export interface Product {
  id: string
  name: string
  price: string
  description: string
  image: string
  slot: number
  categoryId: string
}
export interface ValidateIF {
  required?: {
    value: boolean
    message: string
  }
  minLength?: {
    value: number
    message: string
  }
  maxLength?: {
    value: number
    message: string
  }
  validate?: any
}

export interface ChangeUTF8 {
  e: React.ChangeEvent<HTMLInputElement>
  setValue?: any
  id: string
  props?: any
}

export type InputEvent = React.ChangeEvent<HTMLInputElement>
export type ButtonEvent = React.MouseEvent<HTMLButtonElement>
