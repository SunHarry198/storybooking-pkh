import { server } from '@config/medproSDK'

const localhost = {
  domain: [],
  partnerId: 'umcmono',
  appId: 'umcmono'
}

export const getAppInfo = async ({ ctx }: any) => {
  try {
    if (!ctx) {
      throw new Error('NextJS CTX not found')
    }

    // const host = ctx.req?.rawHeaders[1]
    const host = 'umcmono-testing.medpro.com.vn'
    // const host = 'nextjs-dev.medpro.com.vn'

    if (!host) {
      return localhost
    } else {
      const params = { domain: host.replace(':3013', '') }
      console.info('params get domain: ', params)
      const { data } = await server.getByDomain({ domain: params.domain })
      return data
    }
  } catch (error) {
    console.info(
      'Không có partnerId error :>> localhost',
      localhost,
      (error as any)?.response?.data
    )
    return localhost
  }
}
