import { currentEnv } from '@config/envs'

export const initState = {
  timeCountDown: {
    days: 0,
    hours: 0,
    minutes: 0,
    seconds: 0
  }
}
export const API_BE = currentEnv.API_BE
export const keyMemory = {
  userInfo: 'userInfo',
  analytics: 'analytics',
  partnerId: 'partnerId',
  hostname: 'hostname'
}
export enum ACTION_CSKH {
  BOOK = 'book',
  UPDATE_PATIENT = 'update_patient',
  CREATE_PATIENT = 'create_patient',
  RE_ADD_PATIENT = 're_add_patient',
  HISTORY_BOOKING = 'history_booking'
}
export const listAppId = ['medpro', 'momo', 'vnptpay', 'cskh']
