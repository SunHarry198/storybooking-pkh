export const menu = [
  {
    id: '',
    key: 'benh-vien',
    link: '/benh-vien',
    url: '/benh-vien',
    label: 'Bệnh viện',
    group: 'menuHeader',
    sortOrder: 1,
    status: true
  },
  {
    id: '',
    key: 'phong-mach',
    link: '/phong-mach',
    url: '/phong-mach',
    label: 'Phòng mạch',
    group: 'menuHeader',
    sortOrder: 2,
    status: true
  },
  {
    id: '',
    key: 'bac-si',
    link: '/bac-si',
    url: '/bac-si',
    label: 'Bác sĩ',
    group: 'menuHeader',
    sortOrder: 3,
    status: true
  },
  {
    id: '',
    key: 'tin-tuc',
    link: '/tin-tuc',
    url: '/tin-tuc',
    label: 'Tin tức',
    group: 'menuHeader',
    sortOrder: 4,
    status: true
  },
  {
    id: '',
    key: 'goi-dich-vu',
    link: '/goi-dich-vu',
    url: '/goi-dich-vu',
    label: 'Gói dịch vụ',
    group: 'menuHeader',
    sortOrder: 4,
    status: false
  },
  {
    id: '',
    key: 'tiem-chung',
    link: '/tiem-chung',
    url: '/tiem-chung',
    label: 'Tiêm chủng',
    group: 'menuHeader',
    sortOrder: 5,
    status: true
  },
  {
    id: '',
    key: 'huong-dan',
    link: '/huong-dan',
    url: '/huong-dan',
    label: 'Hướng dẫn',
    group: 'menuHeader',
    sortOrder: 6,
    status: true
  }
]
