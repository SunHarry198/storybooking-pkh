import { clog } from '@lib/support/clog'
import { get } from 'lodash'
import moment from 'moment'
import { NextRouter } from 'next/router'
import { initState } from './constants'

export const fetcherXml = async (url: string) => {
  try {
    await fetch(url)
      .then((response) => response.text())
      .then((text) => console.info(text))
  } catch (error: any) {
    clog({ name: 'fetcherXml', child: error, type: 'error' })
  }
}

export const fetcher = async (url: string, init?: any) => {
  try {
    const res: any = await fetch(url, init)
    return await res?.json()
  } catch (error: any) {
    console.info('error :>> ', error)
    return [getError(error)]
  }
}

export const getError = (error: any) => {
  const e = get(error, 'response.data', '')
  return {
    error: true,
    ...e,
    ...error
  }
}

export const scrollTo = () => {
  return window.scrollTo({
    top: 0,
    behavior: 'smooth'
  })
}

export const money = (str: any) => {
  const parts = str.toString().split('.')
  parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, '.')
  return parts.join('.') + ' ' + 'đ'
}

export const onPush = (txt: string, router: NextRouter) => () => {
  router.push(txt)
}

const handingTime = (item: any) => {
  return item < 10 ? '0' + item : item
}

export const doTimeCountDown = (timeThen: string) => {
  const now: any = moment().utc()
  const then: any = moment(timeThen).utc()

  const times = then.diff(now)

  const seconds = Math.floor((times / 1000) % 60)
  const minutes = Math.floor((times / 1000 / 60) % 60)
  const hours = Math.floor((times / (1000 * 60 * 60)) % 24)
  const days = Math.floor(times / (1000 * 60 * 60 * 24))

  if (times <= 0) {
    return {
      status: true,
      timeCountDown: initState.timeCountDown
    }
  } else {
    const timeCountDown = {
      days: handingTime(days),
      hours: handingTime(hours),
      minutes: handingTime(minutes),
      seconds: handingTime(seconds)
    }
    return {
      status: true,
      timeCountDown
    }
  }
}

export const jsonToQueryString = (obj: { [key: string]: any } = {}): string => {
  return Object.keys(obj)
    .map((key) => {
      return `${encodeURIComponent(key)}=${encodeURIComponent(obj[key])}`
    })
    .join('&')
}
