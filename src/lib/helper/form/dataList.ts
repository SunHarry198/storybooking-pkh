export const dayList = [{ id: 0, name: 'Ngày' }]
for (let i = 1; i <= 31; i++) {
  dayList.push({ id: i, name: i.toString() })
}

export const monthList = [{ id: 0, name: 'Tháng' }]
for (let i = 1; i <= 12; i++) {
  monthList.push({ id: i, name: i.toString() })
}

export const yearList = [{ id: 0, name: 'Năm' }]
for (let i = new Date().getFullYear(); i >= 1900; i--) {
  yearList.push({ id: i, name: i.toString() })
}

export const genderList = [
  {
    id: '1',
    name: 'Nam'
  },
  {
    id: '0',
    name: 'Nữ'
  }
]

export const guideList = [
  {
    id: 'TAI_KHAM',
    name: 'Tái khám theo lịch hẹn'
  },
  {
    id: 'CHUYEN_TUYEN',
    name: 'Chuyển tuyến'
  }
]
