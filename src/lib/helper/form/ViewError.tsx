const Error = ({ errors }: any) => {
  return (
    <p>
      <em style={{ color: 'red' }}>{errors}</em>
    </p>
  )
}

export default Error
