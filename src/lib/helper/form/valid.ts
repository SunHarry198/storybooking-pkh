import { size } from 'lodash'
import moment from 'moment'
import validator from 'validator'

class Valid {
  required = ({ required, name }: { required: boolean; name: string }) => {
    return {
      required: {
        value: required,
        message: name + ' không được trống!'
      }
    }
  }
  fullName = () => {
    return {
      validate: {
        pattern: (value: any) => {
          if (!value) {
            return 'Vui lòng nhập họ và tên!'
          } else {
            const getFullname = value
              .split(' ')
              .filter((item: any) => item !== '')

            if (!getFullname.find((item: any) => item.length > 1)) {
              return 'Phải bao gồm họ và tên!'
            }
            if (size(getFullname) < 2) {
              return 'Phải bao gồm họ và tên!'
            }
            if (size(getFullname) > 6) {
              return 'Họ và tên quá dài!'
            }
          }
        }
      }
    }
  }
  phone = () => {
    return {
      validate: {
        pattern: (value: any) => {
          if (!value) {
            return 'Vui lòng nhập số điện thoại!'
          } else {
            const getPhone = value.split(' ').filter((item: any) => item !== '')
            if (size(getPhone) < 7 || size(getPhone) > 14) {
              return 'Vui lòng nhập đúng định dạng!'
            }
          }
        }
      }
    }
  }
  password = () => {
    return {
      validate: {
        pattern: (value: any) => {
          if (!value) {
            return 'Vui lòng nhập mật khẩu!'
          } else {
            if (size(value) < 6) {
              return 'Mật khẩu quá ngắn!'
            }
          }
        }
      }
    }
  }

  birthYear = () => {
    return {
      validate: {
        pattern: (value: any) => {
          if (size(value) !== 4) {
            return 'Vui lòng nhập đúng năm sinh! Ví dụ: 1998'
          }
        }
      }
    }
  }

  birthdate = () => {
    return {
      validate: {
        required: (value: any) => {
          if (value) {
            if (value === '__/__/20__') {
              return 'Vui lòng nhập ngày tháng năm !'
            }
          } else {
            return 'Vui lòng nhập ngày tháng năm !'
          }
        },
        pattern: (value: any) => {
          const curDate = moment()
          const curYear = curDate.year()

          if (value) {
            if (size(value) <= 4) {
              const age = curYear - Number(value)
              if (age > 120 || isNaN(age)) {
                return 'Vui lòng nhập đúng định dạng ngày tháng! Ví dụ: 19/07/2021'
              }
            } else {
              const date = moment(value, 'DD/MM/YYYY')
              if (!date.isValid()) {
                return 'Vui lòng nhập đúng định dạng ngày tháng! Ví dụ: 19/07/2021'
              }
            }
          }
        }
      }
    }
  }

  BHYTDate = () => {
    return {
      validate: {
        pattern: (value: any) => {
          const curDate = moment()
          const curYear = curDate.year()

          if (value) {
            if (size(value) <= 4) {
              const age = curYear - Number(value)
              if (age > 120 || isNaN(age)) {
                return 'Vui lòng nhập đúng định dạng ngày tháng! Ví dụ: 19/07/2021'
              }
            } else {
              const date = moment(value, 'DD/MM/YYYY')
              if (!date.isValid()) {
                return 'Vui lòng nhập đúng định dạng ngày tháng! Ví dụ: 19/07/2021'
              }
            }
          }
        }
      }
    }
  }

  mobile = (isChildren: boolean) => {
    return {
      required: {
        value: !isChildren,
        message: 'Vui lòng nhập Số điện thoại!'
      },
      validate: {
        pattern: (value: any) => {
          if (value) {
            if (!validator.isMobilePhone(value, 'vi-VN')) {
              return 'Số điện thoại không đúng!'
            }
          }
        }
      }
    }
  }

  email = () => {
    return {
      validate: {
        pattern: (value: string) => {
          if (!validator.isEmail(value) && value) {
            return 'Email không đúng định dạng!'
          }
        }
      }
    }
  }

  sex = () => {
    return {
      validate: {
        required: (value: any) => {
          if (value === 'default') {
            return 'Vui lòng chọn giới tính!'
          }
        }
      }
    }
  }

  career = () => {
    return {
      validate: {
        required: (value: any) => {
          if (!value) return 'Vui lòng chọn nghề nghiệp!'
          if (value === 'default') return 'Vui lòng chọn nghề nghiệp!'
        }
      }
    }
  }

  city = () => {
    return {
      validate: {
        required: (value: any) => {
          if (!value) return 'Vui lòng chọn Tỉnh / Thành!'

          if (value === 'default') return 'Vui lòng chọn Tỉnh / Thành!'
        }
      }
    }
  }

  district = () => {
    return {
      validate: {
        required: (value: any) => {
          if (!value) return 'Vui lòng chọn Quận / Huyện!'

          if (value === 'default') return 'Vui lòng chọn Quận / Huyện!'
        }
      }
    }
  }

  ward = () => {
    return {
      validate: {
        required: (value: any) => {
          if (!value) return 'Vui lòng chọn Phường / Xã!'

          if (value === 'default') return 'Vui lòng chọn Phường / Xã!'
        }
      }
    }
  }

  address = () => {
    return {
      required: {
        value: true,
        message: 'Vui lòng nhập chi tiết Địa chỉ!'
      }
    }
  }

  CMND = () => {
    return {
      validate: {
        pattern: (value: any) => {
          if (value) {
            if (![8, 9, 12].includes(value.length)) {
              return 'Vui lòng nhập đúng thông tin!'
            }
          }
        }
      }
    }
  }

  relativeType = () => {
    return {
      validate: {
        required: (value: any) => {
          if (!value) return 'Vui lòng chọn quan hệ bệnh nhân!'
          if (value === 'default') {
            return 'Vui lòng chọn quan hệ bệnh nhân!'
          }
        }
      }
    }
  }
}

export { Valid }
