import { ENV } from '@config/envs'
import { server } from '@config/medproSDK'
import { NameEnv, urlBE, URL_BO } from '@init/init'
const listTesting = [
  'testing',
  'localhost',
  '192.168.0.122',
  'internal-medpro.ddns.net'
]

export const handleURL_BO = (host: any) => {
  if (!host) {
    return URL_BO.LIVE
  }

  const isTesting = findEnv({
    list: listTesting,
    _hostname: host
  })
  if (isTesting) {
    return URL_BO.TESTING
  } else {
    return URL_BO.LIVE
  }
}

const findEnv = ({ list = [], _hostname }: any) => {
  if (_hostname) {
    return list.find((item: any) => _hostname.includes(item))
  }
  return null
}

export const handleurlBE = ({
  hostname,
  ctx
}: {
  hostname?: string
  ctx?: any
}) => {
  const _hostname = hostname ? hostname : ctx?.req?.rawHeaders[1]

  let api: any

  if (ENV !== NameEnv.PRODUCTION) {
    const isTesting = findEnv({
      list: listTesting,
      _hostname
    })
    const isBeta = findEnv({
      list: ['-beta'],
      _hostname
    })
    const isHotfix = findEnv({
      list: ['-hotfix'],
      _hostname
    })

    if (isTesting) {
      api = urlBE.TESTING
    }

    if (isBeta) {
      api = urlBE.BETA
    }

    if (isHotfix) {
      api = urlBE.HOTFIX
    }
  } else {
    api = urlBE.LIVE
  }
  server.setDomain(api)
}
