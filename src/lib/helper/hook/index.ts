import { AppState } from '@store/interface/appState'
import { TypedUseSelectorHook, useSelector } from 'react-redux'

export const useAppSelector: TypedUseSelectorHook<AppState> = useSelector

export const headerSdk: HeaderSdk = () => {
  return {
    // partnerid: hospital?.partnerId || total?.appId,
    // token: user?.info?.token,
    // platform: total?.device?.platform || 'pc',
    // appid: total?.appId,
    // version: '1'
  }
}

export type HeaderSdk = ({ hospital, user, total }: AppState) => HeadersParams

export interface HeadersParams {
  token?: string
  partnerid?: string
  appid?: string
  momoid?: string
  file?: boolean
  cskhtoken?: string
  platform?: string
  version?: string
}

export const headerAxios = ({ user }: AppState) => {
  console.info(user)
  return {
    // user?.info?.token
    Authorization: 'Bearer ' + 'jkahsjdh'
  }
}
