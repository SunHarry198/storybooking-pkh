import { serviceWorker } from '@lib/method'
import React from 'react'

const useMyApp = () => {
  React.useEffect(() => {
    serviceWorker()
  }, [])
}

export default useMyApp
