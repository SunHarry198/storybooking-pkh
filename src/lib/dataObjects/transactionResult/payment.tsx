import format from '@lib/format'
export const transactionPaymentResult = (item: any) => {
  const data = [
    {
      title: 'Phương thức thanh toán:',
      value: item?.payment?.paymentMethod
    },
    {
      title: 'Cổng thanh toán:',
      value: item?.payment.gatewayId
    },
    {
      title: 'Trạng thái thanh toán:',
      value: item?.bookingInfo?.description
    },
    {
      title: 'Trạng thái đồng bộ:',
      value:
        item?.bookingInfo?.syncStatus === 'success'
          ? 'Đồng bộ'
          : 'Chưa đồng bộ',
      style: {
        color:
          item?.bookingInfo?.syncStatus === 'success' ? '#00c851' : '#ff3547'
      }
    },
    {
      title: 'Tổng tiền khám:',
      value: format.money(item?.payment.subTotal),
      style: {
        color: '#ff3547'
      }
    }
  ]
  return data
}
