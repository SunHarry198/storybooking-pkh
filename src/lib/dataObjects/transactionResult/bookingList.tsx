import format from '@lib/format'

export const bookingListPatient = (item: any) => {
  const data = [
    {
      title: 'Bệnh nhân:',
      value: item?.fullname,
      style: {
        color: '#ff3547'
      }
    },
    {
      title: 'Trạng thái thanh toán:',
      value: item?.status
    }
  ]
  return data
}

export const bookingListPayment = (item: any) => {
  const data = [
    {
      title: 'Ngày khám:',
      value: item?.date
    },
    {
      title: 'Tiền khám:',
      value: format.money(item?.sprice)
    }
  ]
  return data
}
