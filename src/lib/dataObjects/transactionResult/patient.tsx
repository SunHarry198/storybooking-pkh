import moment from 'moment'

export const transactionPatientResult = (item: any) => {
  const data = [
    {
      title: 'Bệnh nhân:',
      value: `${item?.patient?.surname} ${item?.patient?.name}`,
      style: {
        color: '#ff3547'
      }
    },
    {
      title: 'Mã phiếu:',
      value: item?.smsCode
    },
    {
      title: 'Giới tính:',
      value: item?.patient?.sex === 1 ? 'Nam' : 'Nữ'
    },
    {
      title: 'Ngày sinh:',
      value: item?.patient?.birthdate
    },
    {
      title: 'Ngày đặt:',
      value: moment(item?.payment?.paymentTime).format('DD/MM/YYYY HH:mm')
    },
    {
      title: 'Ngày khám:',
      value: moment(item?.date).format('DD/MM/YYYY HH:mm'),
      style: { color: '#00c851' }
    }
  ]
  return data
}
