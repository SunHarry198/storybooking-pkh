import {
  EnvironmentOutlined,
  FileOutlined,
  GiftOutlined,
  ManOutlined,
  PhoneOutlined,
  UserOutlined
} from '@ant-design/icons'

export const patientData = (item: any) => {
  const data = [
    {
      icon: <UserOutlined />,
      title: 'Bệnh nhân',
      value: `${item?.surname} ${item?.name}`,
      style: {
        color: '#ff3547'
      }
    },
    {
      icon: <FileOutlined />,
      title: 'Mã hồ sơ',
      value: item?.code
    },
    {
      icon: <ManOutlined />,
      title: 'Giới tính',
      value: item?.sex === 1 ? 'Nam' : 'Nữ'
    },
    {
      icon: <GiftOutlined />,
      title: 'Ngày sinh',
      value: item?.birthyear
    },
    {
      icon: <PhoneOutlined />,
      title: 'Số điện thoại',
      value: item?.mobile
    },
    {
      icon: <EnvironmentOutlined />,
      title: 'Địa chỉ',
      value:
        item?.address +
        `, ${item?.district?.name || ''}` +
        `, ${item?.city?.name || ''}`
    }
  ]
  return data
}
