import { ComponentMeta, ComponentStory } from '@storybook/react'
import { mockHospitalProps } from './Hospital.mocks'
import HospitalBill, { HospitalProps } from '../index'
import React from 'react'
// const removeTable = {
//   table: {
//     disable: true
//   }
// }
export default {
  title: 'Components/Bill',
  component: HospitalBill,
  // More on argTypes: https://storybook.js.org/docs/react/api/argtypes
  argTypes: {
    backgroundColor: { control: 'color' }
  }
} as ComponentMeta<typeof HospitalBill>

const Template: ComponentStory<typeof HospitalBill> = (args) => (
  <HospitalBill {...args} />
)

export const Hospital = Template.bind({})

Hospital.args = {
  ...mockHospitalProps.Hospital
} as HospitalProps
