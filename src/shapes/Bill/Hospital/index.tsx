import React from 'react'
import styles from './../styles.module.css'
import Barcode from 'react-barcode'

export interface HospitalProps {
  item?: {
    bookingCode?: string
  }
  backgroundColor?: string
}

const HospitalBill: React.FC<HospitalProps> = ({ item, backgroundColor }) => {
  const status = 1
  return (
    <>
      {status === 1 && (
        <>
          <article className={styles.title}>Mã phiếu</article>
          <article style={{ backgroundColor }}>
            <Barcode
              value={item?.bookingCode}
              width='1.4'
              height='45'
              format='CODE128'
              fontSize='15'
            />
          </article>
        </>
      )}
    </>
  )
}

export default HospitalBill
