import React from 'react'
import { BookingInfoProps } from './../../../page-components/Booking/index'
import styles from './../styles.module.css'

const PatientInfo: React.FC<BookingInfoProps> = ({ bookingInfo }: any) => {
  return (
    <>
      <div className={styles.modalContentBill}>
        <ul>
          <li>
            <div>Bệnh nhân:</div>
            <article>{`${bookingInfo?.patient?.surname} ${bookingInfo?.patient?.name}`}</article>
          </li>
          <li>
            <div>Ngày sinh:</div>
            <article>
              {bookingInfo?.patient?.birthdate ||
                bookingInfo?.patient?.birthyear}
            </article>
          </li>
          <li>
            <div>Mã bệnh nhân:</div>
            <article>{bookingInfo?.patient?.code}</article>
          </li>
        </ul>
      </div>
    </>
  )
}

export default PatientInfo
