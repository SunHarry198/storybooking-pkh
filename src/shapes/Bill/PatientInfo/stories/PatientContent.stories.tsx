import { ComponentMeta, ComponentStory } from '@storybook/react'
import { mockBookingContentProps } from './PatientContent.mocks'
import { BookingInfoProps } from './../../../../page-components/Booking/index'
import PatientInfo from '../index'
import React from 'react'
// const removeTable = {
//   table: {
//     disable: true
//   }
// }
export default {
  title: 'Components/Bill',
  component: PatientInfo,
  // More on argTypes: https://storybook.js.org/docs/react/api/argtypes
  argTypes: {
    backgroundColor: { control: 'color' }
  }
} as ComponentMeta<typeof PatientInfo>

const Template: ComponentStory<typeof PatientInfo> = (args) => (
  <PatientInfo {...args} />
)

export const PatientContent = Template.bind({})

PatientContent.args = {
  ...mockBookingContentProps.PatientContent
} as BookingInfoProps
