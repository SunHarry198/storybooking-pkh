import Link from 'next/link'
import React from 'react'
import styles from './../styles.module.css'

export interface CopyRightFooterBillProps {
  item?: {
    bookingCode?: string
  }
}

// tagline
const CopyRightFooterBill: React.FC<CopyRightFooterBillProps> = () => {
  return (
    <>
      <Link href=''>
        <a>
          <div className={styles.copyRight}>
            <span>Bản quyền thuộc</span>
            <img
              src='https://bo-api-testing.medpro.com.vn/static/images/medpro/web/header_logo.svg'
              alt='logo'
            />
          </div>
        </a>
      </Link>
      <div className={styles.introMedpro}>
        Đặt lịch khám tại Bệnh viện - Phòng khám hàng đầu Việt Nam
      </div>
    </>
  )
}

export default CopyRightFooterBill
