import { ComponentMeta, ComponentStory } from '@storybook/react'
import { mockBookingProps } from './Booking.mocks'
import BookingBillInfo, { BookingPageProps } from './../index'
import React from 'react'
// const removeTable = {
//   table: {
//     disable: true
//   }
// }
export default {
  title: 'Pages',
  component: BookingBillInfo,
  // More on argTypes: https://storybook.js.org/docs/react/api/argtypes
  argTypes: {
    backgroundColor: { control: 'color' },
    backgroundContent: { control: 'color' },
    backgroundDescription: { control: 'color' }
  }
} as ComponentMeta<typeof BookingBillInfo>

const Template: ComponentStory<typeof BookingBillInfo> = (args) => (
  <BookingBillInfo {...args} />
)

export const BookingInfo = Template.bind({})

BookingInfo.args = {
  ...mockBookingProps.BookingInfo
} as BookingPageProps
