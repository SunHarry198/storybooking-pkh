import { urlBE, urlLogin, urlMCO, urlSupportCSKH } from '@init/init'

export const API_MCO = urlMCO.PRODUCTION
export const API_LOGIN = urlLogin.PRODUCTION
export const API_BE = urlBE.PRODUCTION
export const URL_SUPPORT_CSKH = urlSupportCSKH.PRODUCTION
