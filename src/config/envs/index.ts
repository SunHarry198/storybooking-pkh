import { NameEnv } from '@init/init'
import includeEnvs from './includeEnvs'
export const ENV: ENV = process.env.ENV || NameEnv.TESTING
export const currentEnv = includeEnvs[ENV]
type ENV = NameEnv
declare let process: {
  env: {
    ENV: NameEnv
  }
}
