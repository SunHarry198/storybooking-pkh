import { NameEnv } from '@init/init'
declare global {
  namespace NodeJS {
    interface ProcessEnv {
      GITHUB_AUTH_TOKEN: string
      NODE_ENV: env
      ENV: env
      PORT?: string
      PWD: string
    }
  }
}

type env = NameEnv
