import HeaderPublic from '@components/layout/HeaderPublic'
import cx from 'classnames'
import React from 'react'
import styles from './styles.module.css'

const HomeLayout = ({ children }: any) => {
  return (
    <div className={styles.layout}>
      <HeaderPublic />
      <main className={cx(styles.content)}>{children}</main>
    </div>
  )
}

export default HomeLayout
