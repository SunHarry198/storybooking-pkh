import Container from '@components/atoms/Container'
import styles from './styles.module.scss'
import cx from 'classnames'
import { Col, Row } from 'antd'

const _Example = () => {
  return (
    <Container className={cx(styles.demo)}>
      <Row>
        <Col>
          <p>Demo</p>
        </Col>
      </Row>
    </Container>
  )
}

export default _Example
