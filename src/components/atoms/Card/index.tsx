import { Card } from 'antd'
import React from 'react'
import cx from 'classnames'
import styles from './styles.module.css'

const CardAntd = ({ children, title, extra, className: classStyles }: any) => {
  return (
    <Card
      title={title}
      extra={extra}
      className={cx(styles.cardAntd, classStyles)}
    >
      {children}
    </Card>
  )
}

export default CardAntd
