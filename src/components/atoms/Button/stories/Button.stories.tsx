import { ComponentMeta, ComponentStory } from '@storybook/react'
import Button, { ButtonPropsCustom } from './../index'
import { mockAuthButtonProps } from './Button.mocks'
const removeTable = {
  table: {
    disable: true
  }
}
export default {
  title: 'UI/Button',
  component: Button,
  // More on argTypes: https://storybook.js.org/docs/react/api/argtypes
  argTypes: {
    backgroundColor: { control: 'color' },
    borderColor: { control: 'color' },
    borderWidth: { control: 'number' },
    borderRadius: { control: 'number' },
    icon: removeTable,
    onClick: removeTable,
    className: removeTable
  }
} as ComponentMeta<typeof Button>

const Template: ComponentStory<typeof Button> = (args) => <Button {...args} />

export const Base = Template.bind({})

Base.args = {
  ...mockAuthButtonProps.base
} as ButtonPropsCustom
