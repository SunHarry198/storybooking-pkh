import { Form } from 'antd'
import React from 'react'
import cx from 'classnames'
import styles from './styles.module.scss'

const FormItem = ({ title, value, className: classStyles, ...props }: any) => {
  if (!value) {
    return null
  }
  return (
    <Form.Item
      className={cx(styles.item)}
      label={
        <>
          {props.icon ? (
            <>
              {props.icon} &nbsp; {title}
            </>
          ) : (
            <>{title}</>
          )}
        </>
      }
    >
      <p className={cx(styles.itemContent, classStyles)} style={props.style}>
        {value}
      </p>
    </Form.Item>
  )
}

export default FormItem
