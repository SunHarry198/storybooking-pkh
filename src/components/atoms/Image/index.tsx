import cx from 'classnames'
import Image from 'next/image'
import React from 'react'
import styles from './styles.module.scss'

export interface Props {
  src: any
  alt?: any
  size?: any
  width?: any
  height?: any
  className?: any
}

const Images = (props: Props) => {
  const { src, alt = '', size = '45', width, height, className } = props

  if (!src) {
    return <p>no src</p>
  }

  return (
    <figure className={cx(className, styles.figureImage)}>
      <Image
        src={src}
        width={width || size}
        height={height || size}
        alt={alt}
      />
    </figure>
  )
}

export default Images
