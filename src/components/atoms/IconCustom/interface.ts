import * as Names from './names'

type Names = typeof Names
type IconName = keyof Names

export interface IconProps {
  name: IconName
  width?: number | string
  fill?: any
  height?: number | string
  children?: any
  stroke?: any
  className?: any
  size?: number | string
}
