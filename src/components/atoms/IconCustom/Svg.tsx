import { IconProps } from './interface'
import styles from './styles.module.scss'
import cx from 'classnames'

export const Svg = ({
  children,
  fill = 'none',
  width = 24,
  size,
  height,
  stroke,
  className
}: IconProps) => {
  const props: any = {
    className,
    width: size || width,
    height: size || height || width,
    xmlns: 'http://www.w3.org/2000/svg',
    fill: fill || 'none'
  }
  if (stroke) {
    props.stroke = stroke || 'currentColor'
  }

  return (
    <span className={cx(styles.icon, className)}>
      <svg {...props}>{children}</svg>
    </span>
  )
}
