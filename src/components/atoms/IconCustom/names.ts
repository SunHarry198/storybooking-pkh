export { default as Address } from './lib/Address'
export { default as Family } from './lib/Family'
export { default as ArrowDown } from './lib/ArrowDown'
export { default as ArrowLeft } from './lib/ArrowLeft'
export { default as ArrowUp } from './lib/ArrowUp'
export { default as Bell } from './lib/Bell'
export { default as BacSi } from './lib/Booking/BacSi'
//// liên quan tới đặt khám
export { default as ChuyenKhoa } from './lib/Booking/ChuyenKhoa'
export { default as DichVu } from './lib/Booking/DichVu'
export { default as GioKham } from './lib/Booking/GioKham'
export { default as NgayKham_active } from './lib/Booking/NgayKham_active'
export { default as NgayKham_default } from './lib/Booking/NgayKham_default'
export { default as Call } from './lib/Call'
export { default as Calling } from './lib/Calling'
export { default as Checked } from './lib/Checked'
export { default as ChildDefault } from './lib/ChildDefault'
export { default as Close } from './lib/Close'
export { default as CoupleDefault } from './lib/CoupleDefault'
export { default as CSKH } from './lib/CSKH'
export { default as CustomDefault } from './lib/CustomDefault'
export { default as DateTime } from './lib/DateTime'
export { default as DatKham } from './lib/DatKham'
export { default as Demo } from './lib/Demo'
export { default as Detail } from './lib/Detail'
export { default as FatherDefault } from './lib/FatherDefault'
export { default as Favourite } from './lib/Favourite'
export { default as GoBack } from './lib/GoBack'
export { default as GoiTongDai } from './lib/GoiTongDai'
export { default as HoSo } from './lib/HoSo'
export { default as Like } from './lib/Like'
export { default as Login } from './lib/Login'
export { default as LogoSuport } from './lib/LogoSuport'
export { default as MeDefault } from './lib/MeDefault'
export { default as Mobile } from './lib/Mobile'
export { default as MobileBanking } from './lib/MobileBanking'
export { default as MotherDefault } from './lib/MotherDefault'
export { default as Note } from './lib/Note'
export { default as Noti } from './lib/Noti'
export { default as Notipayment } from './lib/Notipayment'
export { default as PhieuKham } from './lib/PhieuKham'
export { default as PhoneSupportBill } from './lib/PhoneSupportBill'
export { default as Plus } from './lib/Plus'
export { default as CheckUpdate } from './lib/Profile/CheckUpdate'
export { default as CMND } from './lib/Profile/CMND'
export { default as DanToc } from './lib/Profile/DanToc'
export { default as DiaChi } from './lib/Profile/DiaChi'
export { default as DienThoai } from './lib/Profile/DienThoai'
export { default as Email } from './lib/Profile/Email'
export { default as GioiTinh } from './lib/Profile/GioiTinh'
//// liên quan tới profile
export { default as HovaTen } from './lib/Profile/Hovaten'
export { default as NgaySinh } from './lib/Profile/NgaySinh'
export { default as NgheNghiep } from './lib/Profile/NgheNghiep'
export { default as TaoHoSo } from './lib/Profile/TaoHoSo'
export { default as Search } from './lib/Search'
export { default as SearchPlus } from './lib/SearchPlus'
export { default as Service } from './lib/Service'
export { default as Share } from './lib/Share'
export { default as Specialist } from './lib/Specialist'
export { default as Stethoscope } from './lib/Stethoscope'
export { default as StickComplete } from './lib/StickComplete'
export { default as ThanhToan } from './lib/ThanhToan'
export { default as ThanhToanHo } from './lib/ThanhToanHo'
export { default as ThanhToanThatBai } from './lib/ThanhToanThatBai'
export { default as TheATM } from './lib/TheATM'
export { default as TheKhamBenh } from './lib/TheKhamBenh'
export { default as User } from './lib/User'
export { default as ViDienTu } from './lib/ViDienTu'
export { default as XacNhan } from './lib/XacNhan'
export { default as Menu } from './lib/Menu'
