export default function index({ stroke = '#7B8794' }) {
  // #7B8794
  return (
    <svg viewBox='0 0 10 6' fill='none' xmlns='http://www.w3.org/2000/svg'>
      <path
        d='M8.97862 1L5.23394 5L1.48926 1'
        stroke={stroke}
        strokeWidth='2'
        strokeLinecap='round'
        strokeLinejoin='round'
      />
    </svg>
  )
}
