export default function index() {
  return (
    <svg viewBox='0 0 16 16'>
      <path
        fillRule='evenodd'
        clipRule='evenodd'
        d='M8 0C12.4182 0 16 3.58182 16 8C16 12.4182 12.4182 16 8 16C3.58182 16 0 12.4182 0 8C0 3.58182 3.58182 0 8 0Z'
        fill='#FFB340'
      />
      <path
        fillRule='evenodd'
        clipRule='evenodd'
        d='M7 12C7 11.4476 7.44761 11 8 11C8.55239 11 9 11.4476 9 12C9 12.5524 8.55239 13 8 13C7.44761 13 7 12.5524 7 12Z'
        fill='white'
      />
      <path
        fillRule='evenodd'
        clipRule='evenodd'
        d='M8 3C8.55228 3 9 3.34822 9 3.77778V9.22222C9 9.65178 8.55228 10 8 10C7.44772 10 7 9.65178 7 9.22222V3.77778C7 3.34822 7.44772 3 8 3Z'
        fill='white'
      />
    </svg>
  )
}
