export default function index() {
  return (
    <>
      <path
        fillRule='evenodd'
        clipRule='evenodd'
        d='M11.6518 15.6087L5.85472 11.9856L0.0576172 15.6087V0.391357H11.6518V15.6087Z'
      />
    </>
  )
}
