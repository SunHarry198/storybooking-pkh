import * as React from 'react'
import { IconProps } from './interface'
import { Svg } from './Svg'

export const Icon: React.FC<IconProps> = (props) => {
  // const Detail = require(`./lib/${props.name}`)?.default

  return (
    <Svg {...props}>
      {/* <Detail {...props} /> */}
      icon
    </Svg>
  )
}
