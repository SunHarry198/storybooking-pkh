import { IconProps } from '../interface'

export default function index(props: IconProps) {
  return (
    <svg {...props} viewBox='0 0 16 16'>
      <path d='m7.25 3.75-4.5 4.5 4.5 4.5m6-4.5h-10.5' />
    </svg>
  )
}
