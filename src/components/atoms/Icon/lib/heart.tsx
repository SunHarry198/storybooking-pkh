import { IconProps } from '../interface'

export default function index(props: IconProps) {
  return (
    <svg {...props} viewBox='0 0 16 16'>
      <path
        xmlns='http://www.w3.org/2000/svg'
        d='m3.25 9.75c3 3.5 4.75 4.5 4.75 4.5s1.75-1 4.75-4.5 1-7-1.5-7-3.25 3-3.25 3-.75-3-3.25-3-4.5 3.5-1.5 7z'
      />
    </svg>
  )
}
