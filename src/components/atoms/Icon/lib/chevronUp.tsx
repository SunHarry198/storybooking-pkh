import { IconProps } from '../interface'

export default function index(props: IconProps) {
  return (
    <svg {...props} viewBox='0 0 16 16'>
      <path d='m12.25 10.25-4.25-4.5-4.25 4.5' />
    </svg>
  )
}
