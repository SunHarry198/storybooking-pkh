import { IconProps } from '../interface'

export default function index(props: IconProps) {
  return (
    <svg {...props} viewBox='0 0 16 16'>
      <rect height='10.5' width='10.5' y='2.75' x='2.75' />
      <path d='m3.75 12.25 5.5-4.5 3.5 2' />
      <circle cx='5.75' cy='5.75' r='0.5' fill='currentColor' />
    </svg>
  )
}
