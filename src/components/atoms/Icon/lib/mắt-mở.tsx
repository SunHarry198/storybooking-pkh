import { IconProps } from '../interface'

export default function index(props: IconProps) {
  return (
    <svg viewBox='0 0 16 16' {...props}>
      <path d='m1.75 8s2-4.25 6.25-4.25 6.25 4.25 6.25 4.25-2 4.25-6.25 4.25-6.25-4.25-6.25-4.25z' />
      <circle cx='8' cy='8' r='1.25' fill='currentColor' />
    </svg>
  )
}
