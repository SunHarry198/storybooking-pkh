import { IconProps } from '../interface'

export default function index(props: IconProps) {
  return (
    <svg {...props} viewBox='0 0 48 48'>
      <path
        d='M23.9917 6L6 6L6 42H24'
        stroke='black'
        strokeWidth='4'
        strokeLinecap='round'
        strokeLinejoin='round'
      />
      <path
        d='M33 33L42 24L33 15'
        stroke='black'
        strokeWidth='4'
        strokeLinecap='round'
        strokeLinejoin='round'
      />
      <path
        d='M16 23.9917H42'
        stroke='black'
        strokeWidth='4'
        strokeLinecap='round'
        strokeLinejoin='round'
      />
    </svg>
  )
}
