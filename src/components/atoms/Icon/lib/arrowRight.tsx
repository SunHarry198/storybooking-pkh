import { IconProps } from '../interface'

export default function index(props: IconProps) {
  return (
    <svg {...props} viewBox='0 0 16 16'>
      <path d='m8.75 3.25 4.5 4.5-4.5 4.5m-6-4.5h10.5' />
    </svg>
  )
}
