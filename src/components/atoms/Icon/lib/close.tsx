import { IconProps } from '../interface'

export default function index(props: IconProps) {
  return (
    <svg {...props} viewBox='0 0 16 16'>
      <path d='m11.25 4.75-6.5 6.5m0-6.5 6.5 6.5' />
    </svg>
  )
}
