import {
  BellFilled,
  BellOutlined,
  ContactsFilled,
  DiffFilled,
  FieldTimeOutlined,
  MobileFilled,
  PoweroffOutlined
} from '@ant-design/icons'
import { Button, Popover } from 'antd'
import Link from 'next/link'
import styles from './styles.module.css'
import { FaUserCircle } from 'react-icons/fa'
import Container from '@components/atoms/Container'

const HeaderPublic = () => {
  const menu = [
    {
      url: '/',
      display: 'Trang chủ'
    },
    {
      url: '/',
      display: 'Giới thiệu'
    },
    {
      url: '/',
      display: 'Quy trình'
    },
    {
      url: '/',
      display: 'Hướng dẫn'
    },
    {
      url: '/',
      display: 'Thắc mắc'
    },
    {
      url: '/',
      display: 'Liên hệ'
    }
  ]

  const user = {
    token: 'hadkhkasdjka128uj',
    fullName: 'Test nè'
  }
  const handleLogin = () => {
    console.info('login')
  }
  const handleLogout = () => {
    console.info('logout')
  }
  return (
    <>
      <header className={styles.header}>
        <Container className={styles.container}>
          <div className={styles.headerLogo}>
            <Link href='/'>
              <a>
                <img
                  src='https://bo-api-testing.medpro.com.vn/static/images/medpro/web/header_logo.svg'
                  alt='logo'
                />
              </a>
            </Link>
          </div>
          <div className={styles.headerNavbar}>
            <div className={styles.headerNavbarTop}>
              <div className={styles.notification}>
                <Popover
                  placement='bottom'
                  style={{ padding: 0 }}
                  content={
                    <div className={styles.popoverNotification}>
                      <div className={styles.title}>Danh sách thông báo</div>
                      <ul>
                        <li>
                          <Link href='/'>
                            <a>
                              <div className={styles.description}>
                                Bạn đã đăng ký khám bệnh thành công tại Bệnh
                                viện Da Liễu TP.HCM. Mã phiếu: T2206073HIJR5
                              </div>
                              <div className={styles.timeAgo}>
                                <FieldTimeOutlined /> 1 ngày trước
                              </div>
                            </a>
                          </Link>
                        </li>
                        <li>
                          <Link href='/'>
                            <a>
                              <div className={styles.description}>
                                Bạn đã đăng ký khám bệnh thành công tại Bệnh
                                viện Da Liễu TP.HCM. Mã phiếu: T2206073HIJR5
                              </div>
                              <div className={styles.timeAgo}>
                                <FieldTimeOutlined /> 1 ngày trước
                              </div>
                            </a>
                          </Link>
                        </li>
                      </ul>
                      <Link href='/'>
                        <a>
                          <div className={styles.viewMore}>Xem tất cả</div>
                        </a>
                      </Link>
                    </div>
                  }
                  trigger='click'
                >
                  <Button type='primary' className={styles.btnNotification}>
                    <BellOutlined />
                    <section>2</section>
                  </Button>
                </Popover>
              </div>
              <div className={styles.headerNavbarTopDownloadApp}>
                <Button type='primary'>
                  <MobileFilled /> &nbsp;Tải ứng dụng
                </Button>
              </div>
              <div className={styles.headerNavbarAuth}>
                {user?.token ? (
                  <>
                    <Popover
                      placement='bottom'
                      content={
                        <div className={styles.popoverAuth}>
                          <div className={styles.title}>
                            <label>
                              <FaUserCircle />
                            </label>

                            <article>
                              Xin chào <br />
                              <span>{user?.fullName}</span>
                            </article>
                          </div>
                          <ul>
                            <li>
                              <Link href='/'>
                                <a>
                                  <ContactsFilled />
                                  <span>Hồ sơ bệnh nhân</span>
                                </a>
                              </Link>
                            </li>
                            <li>
                              <Link href='/'>
                                <a>
                                  <DiffFilled />
                                  <span>Phiếu khám bệnh</span>
                                </a>
                              </Link>
                            </li>
                            <li>
                              <Link href='/'>
                                <a>
                                  <BellFilled />
                                  <span>Thông báo</span>
                                </a>
                              </Link>
                            </li>
                            <li>
                              <Button type='primary' onClick={handleLogout}>
                                <PoweroffOutlined />
                                <span>Đăng xuất</span>
                              </Button>
                            </li>
                          </ul>
                        </div>
                      }
                      trigger='click'
                    >
                      <Button type='primary'>
                        <FaUserCircle /> &nbsp;
                        {user?.fullName}
                      </Button>
                    </Popover>
                  </>
                ) : (
                  <Link href='/login'>
                    <Button type='primary' onClick={handleLogin}>
                      <PoweroffOutlined /> &nbsp;Đăng nhập
                    </Button>
                  </Link>
                )}
              </div>
            </div>
            <div className={styles.headerNavbarMenu}>
              <ul>
                {menu.map((item, index) => (
                  <li key={index}>
                    <Link href={item.url}>
                      <a>
                        <article>{item.display}</article>
                      </a>
                    </Link>
                  </li>
                ))}
              </ul>
            </div>
          </div>
          <div className={styles.headerSupport}>
            <Link href='/'>
              <a>
                <div className={styles.headerSupportWrapper}>
                  <div className={styles.headerSupportImage}>
                    <img
                      src='https://cs-testing.medpro.com.vn/static/media/chat.dba318df.svg'
                      alt=''
                    />
                  </div>
                  <div className={styles.headerSupportPhone}>
                    <div>Bạn cần trợ giúp?</div>
                    <section>1900 - 2115</section>
                  </div>
                </div>
              </a>
            </Link>
          </div>
        </Container>
      </header>
    </>
  )
}
export default HeaderPublic
