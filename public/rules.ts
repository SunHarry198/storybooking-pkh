export const rules = {
  phone: {
    required: {
      value: true,
      message: 'Vui lòng nhập số điện thoại'
    },
    minLength: {
      value: 9,
      message: 'Vui lòng nhập đúng định dạng'
    },
    maxLength: {
      value: 12,
      message: 'Vui lòng nhập đúng định dạng'
    }
  },
  search: {},
  name:{
    required: {
      value: true,
      message: 'Vui lòng nhập tên'
    },
    minLength: {
      value: 5,
      message: 'Vui lòng nhập đúng định dạng'
    },
  }
}
