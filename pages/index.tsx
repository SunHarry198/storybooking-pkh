import { Page } from '@type/page'
import { Button } from 'antd'
// import dynamic from 'next/dynamic'
// const HomeLayout = dynamic(() => import('@templates/Home'))

interface HomePageProps {
  data: any
}

const HomePage: Page<HomePageProps> = () => {
  return (
    <>
      <Button></Button>
      home page
    </>
  )
}

// HomePage.layout = HomeLayout
export default HomePage
