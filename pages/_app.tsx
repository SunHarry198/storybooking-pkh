import ProtectedPage from '@components/atoms/Author'
import { scrollById } from '@lib/func'
import useDevice from '@lib/helper/hook/useDevice'
import { saveState } from '@lib/helper/localstorage'
import { store, wrapper } from '@store/rootStore'
import DefaultLayout from '@templates/Default'
import { Page } from '@type/page'
import { ConfigProvider } from 'antd'
import 'antd/dist/antd.css'
import { debounce } from 'lodash'
import Head from 'next/head'
import { useRouter } from 'next/router'
import React, { Fragment } from 'react'
import { Provider, useStore } from 'react-redux'
import 'src/assets/styles/app.scss'
import useMyApp from '@lib/helper/hook/useMyApp'
import { getAppInfo } from '@lib/utils/partner'
import { AppContext } from 'next/app'
import { serializeCookies } from '@lib/helper/cookies'
import { handleurlBE } from '@lib/helper/env'
import { getTypeOS } from '@lib/method'
store.subscribe(
  debounce(() => {
    saveState(store.getState())
  })
)

const MyApp = ({ Component, pageProps, ...initProps }: Page) => {
  const store = useStore()
  const router = useRouter()

  const Layout = Component?.layout || DefaultLayout || Fragment

  React.useEffect(() => {
    if (router.query.landing) {
      scrollById({ Id: router.query.landing as string })
    }
  }, [router.query.landing])

  useMyApp()

  const { loading } = useDevice()
  console.info(loading)
  const site = (
    <Layout>
      <Component {...pageProps} {...initProps} />
    </Layout>
  )

  return (
    <React.Fragment>
      <Head>
        <meta name='viewport' content='viewport-fit=cover' />
        <meta content='width=device-width, initial-scale=1' name='viewport' />
      </Head>

      <Provider store={store}>
        <ConfigProvider csp={{ nonce: 'YourNonceCode' }}>
          {Component.requireAuth ? <ProtectedPage>{site}</ProtectedPage> : site}
        </ConfigProvider>
      </Provider>
    </React.Fragment>
  )
}

MyApp.getInitialProps = async (context: AppContext) => {
  const { ctx } = context
  handleurlBE({ ctx })
  const appInfo = await getAppInfo(context)
  await serializeCookies({ ctx, key: 'appInfo', value: appInfo })
  const userAgent = ctx?.req?.headers['user-agent']
  const hostname = ctx?.req?.rawHeaders[1]

  const returnObj: any = {
    appInfo,
    hostname,
    userAgent,
    domain: 'https://' + hostname,
    typeOs: getTypeOS(userAgent)
  }

  return returnObj
}

export default wrapper.withRedux(MyApp)
