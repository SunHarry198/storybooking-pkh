import Document, {
  DocumentContext,
  Head,
  Html,
  Main,
  NextScript
} from 'next/document'

class CustomDocument extends Document {
  static async getInitialProps(ctx: DocumentContext): Promise<any> {
    const initialProps = await Document.getInitialProps(ctx)
    return {
      ...initialProps
    }
  }

  render(): JSX.Element {
    return (
      <Html lang='vi'>
        <Head>
          <meta charSet='utf-8' />
          <meta content='IE=edge' />
          <meta name='theme-color' content='#00b5f1' />

          <link rel='shortcut icon' href='/favicon/medpro.png' />
          <link rel='apple-touch-icon' href='/favicon/medpro.png' />
          <link rel='preconnect' href='https://fonts.googleapis.com' />
          <link rel='manifest' href='/manifest.json' />

          <link rel='preconnect' href='https://fonts.googleapis.com' />
          <link
            rel='preconnect'
            href='https://fonts.gstatic.com'
            crossOrigin='true'
          />
          {/* <link href="https://fonts.googleapis.com/css2?family=Gluten:wght@100;200;300;400;500;600;700;800;900&family=Roboto:ital,wght@0,100;1,100&display=swap" rel="stylesheet"></link> */}

          <link
            href='https://fonts.googleapis.com/css2?family=Josefin+Sans:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;1,100;1,200;1,300;1,400;1,500;1,600;1,700&display=swap'
            rel='stylesheet'
          ></link>
        </Head>

        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    )
  }
}
export default CustomDocument
