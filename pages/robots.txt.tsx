import { GetServerSideProps } from 'next'

function Robots() {
  return <>Im robots</>
}

export default Robots

export const getServerSideProps: GetServerSideProps = async (ctx) => {
  const { req, res } = ctx

  const host = req.rawHeaders[1]
  res.setHeader('Content-Type', 'text/plain')
  res.write(`
      User-agent: *
      Host: ${host}
      Allow: /*
      Disallow:
      Sitemap: ${host}/sitemap
`)
  res.end()
  return {
    props: {}
  }
}
